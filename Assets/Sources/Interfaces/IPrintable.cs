﻿
namespace FizzBuzz
{
    public interface IPrintable
    {
        void Print(FizzBuzzData data);
    }
}