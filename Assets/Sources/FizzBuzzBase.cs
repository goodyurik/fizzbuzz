﻿using UnityEngine;

namespace FizzBuzz
{
    public abstract class FizzBuzzBase: MonoBehaviour
    {
        protected abstract int GenerateRandom();
        protected abstract string GenerateMessage(int value);
    }
}
