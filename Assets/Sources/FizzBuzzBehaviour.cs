﻿using UnityEngine;

namespace FizzBuzz 
{
    public class FizzBuzzBehaviour : FizzBuzzBase, IClickable
    {
        [SerializeField] private FizzBuzzViewBehaviour fizzBuzzView;

        public void OnCLick()
        {
            var randomValue = GenerateRandom();
            var message = GenerateMessage(randomValue);
            fizzBuzzView.Print(new FizzBuzzData(message, randomValue));
        }

        protected override string GenerateMessage(int value)
        {
            var message = string.Empty;

            if (value % 3 == 0) message = "Fizz";
            if (value % 5 == 0) message = "buzz";
            if (value % 3 == 0 && value % 5 == 0) message = "FizzBuzz";

            return message;
        }

        protected override int GenerateRandom()
        {
            return Random.Range(0,101);
        }
    }

    public class FizzBuzzData 
    {
        public int RandomValue { get; }
        public string Message { get; }

        public FizzBuzzData(string message, int randomValue) 
        {
            Message = message;
            RandomValue = randomValue;
        }
    }
}
