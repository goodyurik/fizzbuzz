﻿using UnityEngine;
using UnityEngine.UI;

namespace FizzBuzz
{
    public class FizzBuzzViewBehaviour : MonoBehaviour, IPrintable
    {
        [SerializeField] private Text fizzBuzzText;
        [SerializeField] private Text buttonText;

        public void Print(FizzBuzzData data)
        {
            fizzBuzzText.text = data.Message;
            buttonText.text = data.RandomValue.ToString();
        }
    }
}
